import React, { Component } from 'react'
import axios from 'axios'
import './assets/css/bootstrap.min.css'
import './assets/css/fontawesome.min.css'
import './App.css'

class App extends Component{
    constructor(props){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeEmailLogin = this.handleChangeEmailLogin.bind(this)
        this.handleChangePasswordLogin = this.handleChangePasswordLogin.bind(this)
        this.handleChangeEmailRegister = this.handleChangeEmailRegister.bind(this)
        this.handleChangePasswordRegister = this.handleChangePasswordRegister.bind(this)

        this.state = {
            emailLogin: "",
            passwordLogin: "",
            emailRegister: "",
            passwordRegister: ""
        }
    }

    handleChangeEmailLogin(e){
        this.setState({emailLogin: e.target.value})
    }

    handleChangePasswordLogin(e){
        this.setState({passwordLogin: e.target.value})
    }

    handleChangeEmailRegister(e){
        this.setState({emailRegister: e.target.value})
    }

    handleChangePasswordRegister(e){
        this.setState({passwordRegister: e.target.value})
    }

    handleSubmit(e){
        let data = {
            email: this.state.emailLogin,
            password: this.state.passwordLogin
        }
        e.preventDefault()

        // const data = new FormData(e.target)
        console.log(data)


        axios({
            method: 'post',
            url: 'http://localhost:3000/login',
            data: data,
            config: { headers: {'Content-Type': 'application/json' }}
        })
        .then(function (response) {
            alert("login success!")
            localStorage.setItem('keyToken', `Bearer ${response.data.token}`)
            window.location.href = "http://localhost:3006"
            // headers: {
            //   'authorization': `Bearer ${response.data.token}`,
            //   'Accept' : 'application/json',
            //   'Content-Type': 'application/json'
            // }
        })
    }

    handleSignUp(e){
        e.preventDefault()
        let data = {
            email: this.state.emailRegister,
            password: this.state.passwordRegister
        }

        // const data = new FormData(e.target)
        console.log(e)

        axios({
            method: 'post',
            url: 'http://localhost:3000/register',
            data: data,
            config: { headers: {'Content-Type': 'application/json' }}
        })
        .then(res => {
            if (res.status === 200) {
                window.location.href = "http://localhost:3006"
            } else if (res.status === 500) {
                alert("username is already exist")
            }
        })
    }

    render () {
        return (
            <div className="row">
                <div className="col-md-4">
                </div>
                <div className="col-md-4">
                    <div className="card m-5">
                        <article className="card-body">
                            <a
                                href
                                className="float-right btn btn-outline-primary"
                                data-target="#signUpModal"
                                data-toggle="modal">
                                Sign up
                            </a>
                            <h4 className="card-title mb-4 mt-1">
                                Sign in
                            </h4>
                            <form onSubmit = {this.handleSubmit}>
                                <div className="form-group">
                                    <label>
                                        Username
                                    </label>
                                    <input
                                        value = {this.state.emailLogin}
                                        onChange={this.handleChangeEmailLogin}
                                        name = "username"
                                        className="form-control"
                                        placeholder="Username"
                                        type="text" />
                                </div>
                                <div className="form-group">
                                    <label>
                                        password
                                    </label>
                                    <input
                                        value = {this.state.passwordLogin}
                                        onChange={this.handleChangePasswordLogin}
                                        name="password"
                                        className="form-control"
                                        placeholder="******"
                                        type="password" />
                                </div>
                                <div className="form-group">
                                    <div className="checkbox">
                                        <label> <input type="checkbox" /> Save password </label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <button
                                        type="submit"
                                        className="btn btn-primary btn-block"> Login</button>
                                </div>
                            </form>
                        </article>
                    </div>
                </div>
                <div className="col-md-4">
                </div>


                <div
                    className="modal fade"
                    id="signUpModal"
                    tabIndex={-1}
                    role="dialog"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5
                                    className="modal-title"
                                    id="exampleModalLabel">
                                    Register
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form onSubmit={this.handleSignUp}>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label
                                            htmlFor="username"
                                            className="col-form-label">Username:</label>
                                        <input
                                            value = {this.state.emailRegister}
                                            onChange={this.handleChangeEmailRegister}
                                            type="text"
                                            className="form-control"
                                            id="username"
                                            name="email"/>
                                    </div>
                                    <div className="form-group">
                                        <label
                                            htmlFor="password"
                                            className="col-form-label">Password:</label>
                                        <input
                                            value = {this.state.passwordRegister}
                                            onChange={this.handleChangePasswordRegister}
                                            type="password"
                                            className="form-control"
                                            id="password"
                                            name="password"/>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button
                                        type="button"
                                        className="btn btn-warning"
                                        data-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default App
